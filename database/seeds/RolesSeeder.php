<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'id' => 1,
            'name' => 'registrado',
        ]);
        DB::table('roles')->insert([
            'id' => 2,
            'name' => 'socio',
        ]);
        DB::table('roles')->insert([
            'id' => 3,
            'name' => 'administrador',
        ]);
        DB::table('roles')->insert([
            'id' => 4,
            'name' => 'root',
        ]);

    }
}
